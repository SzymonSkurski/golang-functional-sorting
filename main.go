package main

import (
	"fmt"
	"sort"

	"golang.org/x/exp/constraints"
)

type sortDESC[T constraints.Ordered] struct {
	Orderable[T]
}

// Implement sort interface
func (s sortDESC[T]) Len() int           { return s.Orderable.Len() }
func (s sortDESC[T]) Swap(i, j int)      { s.Orderable[i], s.Orderable[j] = s.Orderable[j], s.Orderable[i] }
func (s sortDESC[T]) Less(i, j int) bool { return s.Orderable[i] > s.Orderable[j] }

type sortASC[T constraints.Ordered] struct {
	Orderable[T]
}

// Implement sort interface
func (s sortASC[T]) Len() int           { return s.Orderable.Len() }
func (s sortASC[T]) Swap(i, j int)      { s.Orderable[i], s.Orderable[j] = s.Orderable[j], s.Orderable[i] }
func (s sortASC[T]) Less(i, j int) bool { return s.Orderable[i] < s.Orderable[j] }

type Orderable[T constraints.Ordered] []T

// Implement sort interface
func (n Orderable[T]) Len() int           { return len(n) }
func (n Orderable[T]) Swap(i, j int)      { n[i], n[j] = n[j], n[i] }
func (n Orderable[T]) Less(i, j int) bool { return n[i] < n[j] }

func main() {
	numbers := Orderable[int]{2, 1, 3, 7, 2, 0, 0, 5}
	sort.Sort(sortASC[int]{numbers})

	fmt.Println(numbers)

	fmt.Println("numbers is sorted:", sort.IsSorted(sortASC[int]{numbers}))

	sort.Sort(sortDESC[int]{numbers})

	fmt.Println(numbers)

	fmt.Println("numbers is sorted:", sort.IsSorted(sortDESC[int]{numbers}))

	myStrings := []string{"bar", "var", "foo", "goo"}
	sort.Sort(sortASC[string]{myStrings})

	fmt.Println(myStrings)

	sort.Sort(sortDESC[string]{myStrings})

	fmt.Println(myStrings)

	numbersF := Orderable[float64]{2.0, 1.0, 3.1, 7.2, 2.1, 0.1, 0.2, 5.2}
	sort.Sort(sortASC[float64]{numbersF})

	fmt.Println(numbersF)
}
